"""worldwidefit URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from worldwidefit_main.views import Home, EventView, EventsView, ResultCreatingView, ContactsView, EventResults, GroupView, GroupEventResultsView, ApproveResultView, GlobalResultCreatingView


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', Home.as_view(), name='Home'),
    url(r'^event/(?P<pk>[-_\w]+)/$', EventView.as_view(), name='Event'),
    url(r'^events/$', EventsView.as_view(), name='Events'),
    url(r'^contacts/$', ContactsView.as_view(), name='Contacts'),
    url(r'^result/(?P<pk>[-_\w]+)/$', ResultCreatingView.as_view(), name='Result set'),
    url(r'^event/(?P<pk>[-_\w]+)/results/$', EventResults.as_view(), name='EventResults'),
    url(r'^group/(?P<group_pk>[-_\w]+)/event/(?P<event_pk>[-_\w]+)/$', GroupEventResultsView.as_view(), name='GroupEvent'),
    url(r'^group/(?P<pk>[-_\w]+)/$', GroupView.as_view(), name='Group'),
    url(r'^approve/(?P<pk>[-_\w]+)/$', ApproveResultView.as_view(), name='ApproveResult'),
    url(r'^result/$', ResultCreatingView.as_view(), name='ResultSet'),
    url(r'^global/(?P<pk>[-_\w]+)/$', GlobalResultCreatingView.as_view(), name='ResultSet'),
    url(r'^global/$', GlobalResultCreatingView.as_view(), name='ResultSet'),
]
