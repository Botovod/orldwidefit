# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from worldwidefit_auth.models import SportGroup


admin.site.register(SportGroup)
