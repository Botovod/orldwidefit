# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from worldwidefit_main.models import Organizer, Event
from django.contrib.auth.models import User


class SportGroup(models.Model):
    title = models.CharField('Название', blank=True, null=False, max_length=255, default='')
    users = models.ManyToManyField(User)
    organizer = models.ForeignKey(Organizer)
    events = models.ManyToManyField(Event)

    class Meta:
        verbose_name = 'Спортивная группа'
        verbose_name_plural = 'Спортивные группы'

    def __unicode__(self):
        return self.title
