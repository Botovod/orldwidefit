# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from worldwidefit_main.models import Organizer, Event, Task, Result


admin.site.register(Organizer)
admin.site.register(Event)
admin.site.register(Task)
admin.site.register(Result)
