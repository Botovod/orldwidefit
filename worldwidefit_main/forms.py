# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from worldwidefit_main.models import Result


class ResultCreatingForm(forms.ModelForm):
    class Meta:
        model = Result
        fields = ['result', 'task']
        widgets = {
            'result': forms.TextInput(attrs={'value': '',
                                            'placeholder': "Результат"}),
            'task': forms.HiddenInput(),
        }


class GlobalResultCreatingForm(forms.ModelForm):
    # proof = forms.CharField(widget=forms.TextInput(attrs={'value': '',
    #                                         'placeholder': "Доказательство"}))

    class Meta:
        model = Result
        fields = ['result', 'proof']
        widgets= {
            'proof': forms.TextInput(attrs={'value': '',
                                            'placeholder': "Подтверждение"}),
            'result': forms.TextInput(attrs={'value': '',
                                            'placeholder': "Результат"}),
            'task': forms.HiddenInput(),
        }


class ApproveResultForm(forms.Form):
    result_id = forms.CharField()
    redirect_to = forms.CharField()
