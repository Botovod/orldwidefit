# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-28 18:52
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('worldwidefit_main', '0002_event_ended'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='result',
            options={'ordering': ['-result'], 'verbose_name': '\u0420\u0435\u0437\u0443\u043b\u044c\u0442\u0430\u0442', 'verbose_name_plural': '\u0420\u0435\u0437\u0443\u043b\u044c\u0442\u0430\u0442\u044b'},
        ),
    ]
