# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


class Organizer(models.Model):
    title = models.CharField('Название', max_length=255, default='')
    description = models.TextField('Подробности', default='', blank=True)
    created = models.DateTimeField('Добавлен', auto_now_add=True)

    class Meta:
        verbose_name = 'Организатор'
        verbose_name_plural = 'Организаторы'

    def __unicode__(self):
        return self.title


class Event(models.Model):
    title = models.CharField('Название', max_length=255, default='')
    description = models.TextField('Подробности', default='', blank=True)
    created = models.DateTimeField('Добавлен', auto_now_add=True)
    ended = models.BooleanField('Уже прошло', blank=False, null=False, default=False)
    isGlobal = models.BooleanField('Глобальное', blank=False, null=False, default=False)

    organizer = models.ForeignKey(Organizer, blank=True, null=True)

    class Meta:
        verbose_name = 'Событие'
        verbose_name_plural = 'События'

    def __unicode__(self):
        return self.title


class Task(models.Model):
    title = models.CharField('Название', max_length=255, default='')
    description = models.TextField('Подробности', default='', blank=True)
    created = models.DateTimeField('Добавлен', auto_now_add=True)

    event = models.ForeignKey(Event, blank=True, null=True)

    class Meta:
        verbose_name = 'Задание'
        verbose_name_plural = 'Задания'

    def __unicode__(self):
        return self.title


class Result(models.Model):
    user = models.ForeignKey(User, blank=False, null=False)
    task = models.ForeignKey(Task, blank=False, null=False)
    result = models.CharField('Результат', max_length=255, blank=False, null=False, default='')
    approved = models.BooleanField('Подтверждён', blank=False, null=False, default=False)
    proof = models.CharField('Подтверждение', max_length=512, blank=True, null=False, default='')

    class Meta:
        verbose_name = 'Результат'
        verbose_name_plural = 'Результаты'
        ordering = ['-result',]

    def __unicode__(self):
        return "{} {} {}".format(self.user.username, self.task.title, self.result)
