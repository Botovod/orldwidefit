# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.generic import TemplateView, ListView, DetailView, FormView
from worldwidefit_main.models import Event, Task, Result, User
from worldwidefit_auth.models import SportGroup
from worldwidefit_main.forms import ResultCreatingForm, ApproveResultForm, GlobalResultCreatingForm
from django.shortcuts import render_to_response, reverse, redirect
from django.template.context_processors import csrf
import random

class Home(ListView):
    template_name = 'main.html'
    context_object_name = 'events'
    model = Event

    def get_queryset(self):
        return Event.objects.filter(ended=True)


class EventView(DetailView):
    template_name = 'event.html'
    model = Event


class ContactsView(ListView):
    template_name = 'contacts.html'
    def get_queryset(self):
        return Event.objects.filter(ended=True)

class EventsView(ListView):
    template_name = 'events.html'
    context_object_name = 'events'
    model = Event

    def get_queryset(self):
        return Event.objects.filter(ended=False).order_by('-pk')


class EventResults(DetailView):
    template_name = 'results.html'
    model = Event

    def get_context_data(self, **kwargs):
        ctx = super(EventResults, self).get_context_data(**kwargs)
        ctx['events_list'] = Event.objects.filter(ended=False)
        return ctx


class ResultCreatingView(FormView):

    template_name = 'result.html'
    form_class = ResultCreatingForm
    success_url = '/event/'

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        return super(ResultCreatingView, self).form_valid(form)

    # В случае get запроса, мы будем отправлять просто страницу с контактной формой
    def get(self, request, *args, **kwargs):
        context = {}
        context.update(csrf(request))  # Обязательно добавьте в шаблон защитный токен

        context['result_form'] = ResultCreatingForm(initial={'task': Task.objects.get(id=self.kwargs['pk']).pk})

        return render_to_response(template_name=self.template_name, context=context)

    def post(self, request, *args, **kwargs):
        context = {}

        form = ResultCreatingForm(request.POST)
        if form.is_valid():

            Result.objects.create(task=Task.objects.get(id=request.POST["task"]),
                                  result=request.POST["result"],
                                  user=request.user)
            task = Task.objects.get(id=request.POST["task"])

            return redirect('{0}{1}/'.format(self.get_success_url(), task.event.pk))
        return render_to_response(template_name=self.template_name, context=context)


class GlobalResultCreatingView(ResultCreatingView):
    form_class = GlobalResultCreatingForm

    def get(self, request, *args, **kwargs):
        context = {}
        context.update(csrf(request))  # Обязательно добавьте в шаблон защитный токен

        context['result_form'] = GlobalResultCreatingForm(initial={'task': Task.objects.get(id=self.kwargs['pk']).pk})

        return render_to_response(template_name=self.template_name, context=context)

    def post(self, request, *args, **kwargs):
        context = {}

        form = ResultCreatingForm(request.POST)
        if form.is_valid():
            result = [" минут", " подходов", " километров", "километров", " минут", " подходов", " подходов",
                      " минут", " километров"]

            # for x in range(1, 8):
            #     for y in range(6, 14):
            #         Result.objects.create(task=Task.objects.get(id=y),
            #                               result='{0}{1}'.format(random.randint(0, 10), result[y - 6]),
            #                               proof=request.POST["proof"],
            #                               user=User.objects.get(id=x))
            Result.objects.create(task=Task.objects.get(id=request.POST["task"]),
                                  result=request.POST["result"],
                                  proof=request.POST["proof"],
                                  user=request.user)
            task = Task.objects.get(id=request.POST["task"])

            return redirect('{0}{1}/'.format(self.get_success_url(), task.event.pk))

class ApproveResultView(FormView):
    form_class = ApproveResultForm
    template_name = 'group_event.html'
    success_url = '/'

    def form_valid(self, form):
        if form.is_valid():

            current_result = Result.objects.get(id=int(form.cleaned_data['result_id']))
            current_result.approved = True
            current_result.save()
            return redirect(form.cleaned_data['redirect_to'])
        else:
            self.form_invalid(form)


class GroupView(DetailView):
    model = SportGroup
    template_name = 'group.html'


class GroupEventResultsView(ListView):
    template_name = 'group_event.html'
    context_object_name = 'results'

    def get_queryset(self):
        sp = SportGroup.objects.get(id=self.kwargs['group_pk'])
        return Result.objects.filter(task__event_id=self.kwargs['event_pk'], user_id__in=sp.users.all().values('id'))

    def get_context_data(self, **kwargs):
        ctx = super(GroupEventResultsView, self).get_context_data(**kwargs)
        ctx['event'] = Event.objects.get(id=self.kwargs['event_pk'])
        ctx['group'] = SportGroup.objects.get(id=self.kwargs['group_pk'])
        return ctx
